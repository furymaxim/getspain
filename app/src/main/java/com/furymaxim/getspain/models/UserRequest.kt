package com.furymaxim.getspain.models

import com.google.gson.annotations.SerializedName

class UserRequest constructor(name: String, job: String) {

    @SerializedName("name")
    var name: String? = null
    @SerializedName("job")
    var job: String? = null

    init{
        this.name = name
        this.job = job
    }


}