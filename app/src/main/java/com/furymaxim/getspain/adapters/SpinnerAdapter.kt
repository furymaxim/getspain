package com.furymaxim.getspain.adapters

import android.content.Context
import android.widget.ArrayAdapter

class SpinnerAdapter(context: Context, resource: Int, list: Array<String>) : ArrayAdapter<String>(context, resource, list) {

    override fun isEnabled(position: Int): Boolean {

        return position != 0
    }

}