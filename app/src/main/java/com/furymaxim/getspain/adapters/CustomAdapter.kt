package com.furymaxim.getspain.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.furymaxim.getspain.R

class CustomAdapter(applicationContext: Context?, private val flags: Array<Int>) : BaseAdapter() {

    private val inflater = LayoutInflater.from(applicationContext)

    override fun getCount(): Int {
        return flags.size
    }

    override fun getItem(i: Int): Any {
        return flags[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    override fun getView(i: Int, convertView: View?, parent: ViewGroup): View? {

        val view: View?
        val vh: ListRowHolder

        if (convertView == null) {
            view = inflater.inflate(R.layout.spinner_item_flag,parent,false)
            vh = ListRowHolder(view)
            view.tag = vh
        }else{
            view = convertView
            vh = view.tag as ListRowHolder
        }

        vh.icon.setImageResource(flags[i])

        return view
    }

  /*  init {
        inflater = LayoutInflater.from(applicationContext)
    }

   */
}


private class ListRowHolder(row: View?) {

    val icon = row?.findViewById(R.id.imageView) as ImageView

    /*init {
        this.icon = row?.findViewById(R.id.imageView) as ImageView
    }*/
}