package com.furymaxim.getspain.api

import com.furymaxim.getspain.models.UserRequest
import com.furymaxim.getspain.models.UserResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface APIInterface {

    @POST("/api/users")
    fun createUser(@Body user: UserRequest): Call<UserResponse>

}