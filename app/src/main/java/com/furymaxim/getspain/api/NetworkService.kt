package com.furymaxim.getspain.api

import com.furymaxim.getspain.utils.Conf
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkService{

    private var mRetrofit: Retrofit? = null

    init{
        val loggingInterceptor  = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()

        mRetrofit = Retrofit.Builder()
            .baseUrl(Conf.BASE_URL)

            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    fun getJSONApi():APIInterface{
        return mRetrofit!!.create(APIInterface::class.java)
    }

    companion object{
        private var mInstance: NetworkService? = null

        fun getInstance():NetworkService{

            if (mInstance == null) mInstance = NetworkService()

            return mInstance!!
        }
    }




}