package com.furymaxim.getspain.ui.fragments


import `in`.codeshuffle.typewriterview.TypeWriterListener
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.furymaxim.getspain.R
import com.furymaxim.getspain.adapters.CustomAdapter
import com.furymaxim.getspain.adapters.SpinnerAdapter
import com.furymaxim.getspain.api.NetworkService
import com.furymaxim.getspain.models.UserRequest
import com.furymaxim.getspain.models.UserResponse
import kotlinx.android.synthetic.main.fragment_transfer.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class TransferFragment : Fragment(), TypeWriterListener {

    private val differentSentences = arrayOf("в Салоу","в Комарругу","в Жирону","в любую точку Испании")
    private var i = 1

    private var sharedPreferences: SharedPreferences? = null
    private var mSpinnerInitialized = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_transfer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAnim()

        sharedPreferences = activity!!.getSharedPreferences("myPref", Context.MODE_PRIVATE)

        setupSpinnerPassengers()
        setupSpinnerContactMethod()
        setupSpinnerFlags()

        if(sharedPreferences!!.contains("noCon") && sharedPreferences!!.getBoolean("noCon",false) || sharedPreferences!!.contains("success") && sharedPreferences!!.getBoolean("success",false)){
            editFrom.setText(sharedPreferences!!.getString("from",""))
            editTo.setText(sharedPreferences!!.getString("to",""))
            spinner.setSelection(sharedPreferences!!.getInt("passengers",0))
            spinnerContact.setSelection(sharedPreferences!!.getInt("contactType",0))
            countryCodeSpinner.setSelection(sharedPreferences!!.getInt("flag",0))
            phoneEdit.setText(sharedPreferences!!.getString("phoneNumber",""))
            if(sharedPreferences!!.contains("noCon")) {
                sharedPreferences!!.edit().remove("noCon").apply()
            }else{
                sharedPreferences!!.edit().remove("success").apply()
            }



            countryCodeSpinner.onItemSelectedListener = object : OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                    if (!mSpinnerInitialized) {
                        mSpinnerInitialized = true
                        return
                    }
                    when (position) {
                        0 -> phoneEdit.mask = "+7(###)###-##-##"
                        1 -> phoneEdit.mask = "+34(###)###-##-##"
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
        }

        swap.setOnClickListener {
            val temp:String?
            val fromValue = editFrom.text.toString()
            val toValue = editTo.text.toString()

            temp = toValue
            editTo.setText(fromValue)
            editFrom.setText(temp)


        }

        button.setOnClickListener { if (validateFields()){
            title.visibility = View.GONE
            typeWriterView!!.visibility = View.GONE
            loading!!.visibility = View.VISIBLE
            typeWriterView!!.removeAnimation()
            doRequest("Maxim","IT-programmer")

        } else
            Toast.makeText(context, "Заполните все поля", Toast.LENGTH_SHORT).show() }

        phoneEdit.setOnEditorActionListener(null)

    }



  /*  private fun doRequest(editFromValue,editToValue,spinnerItemValue...){

    }


   */

    private fun doRequest(name:String, job:String){
        NetworkService.getInstance()
            .getJSONApi()
            .createUser(UserRequest(name,job))
            .enqueue(object: Callback<UserResponse> {

                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    call.cancel()

                    sharedPreferences!!.edit()
                        .putBoolean("noCon",true)
                        .putString("from",editFrom.text.toString())
                        .putString("to",editTo.text.toString())
                        .putInt("passengers",spinner.selectedItemPosition)
                        .putInt("contactType",spinnerContact.selectedItemPosition)
                        .putInt("flag",countryCodeSpinner.selectedItemPosition)
                        .putString("phoneNumber",phoneEdit.text.toString())
                        .apply()

                    switchFragment(ErrorFragment())
                }

                override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                    if(response.isSuccessful){

                        sharedPreferences!!.edit()
                            .putBoolean("success",true)
                            .putInt("passengers",spinner.selectedItemPosition)
                            .putInt("contactType",spinnerContact.selectedItemPosition)
                            .putInt("flag",countryCodeSpinner.selectedItemPosition)
                            .putString("phoneNumber",phoneEdit.text.toString())
                            .apply()

                        val user = response.body()

                        switchFragment(SuccessFragment())

                      //  Toast.makeText(context, "Created a new user.\nInfo:\nid: ${user!!.id}\nname: ${user.name}\njob: ${user.job}\ncreated at: ${user.createdAt}",Toast.LENGTH_SHORT).show()

                    }else{
                        //если пришла сервера неудача
                    }

                    call.cancel()
                }
            })
    }

    override fun onTypingStart(text: String?) {
    }

    override fun onTypingRemoved(text: String?) {

    }

    override fun onTypingEnd(text: String?) {

        Handler().postDelayed({
            getStringToPrint(i)
            i++
            if(i == 4) i = 0

        },2000)

    }

    override fun onCharacterTyped(text: String?, position: Int) {

    }

    private fun initAnim(){

        try {
            typeWriterView!!.setTypeWriterListener(this)
            typeWriterView!!.setWithMusic(false)
            typeWriterView!!.setDelay(150)
            i = 1
            getStringToPrint(0)
        }catch (e: KotlinNullPointerException){
            e.printStackTrace()
        }


    }


    private fun getStringToPrint(i:Int){
        try{
            typeWriterView!!.removeAnimation()
            typeWriterView!!.animateText(differentSentences[i])
        }catch (e:KotlinNullPointerException){
            e.printStackTrace()
        }
    }


    private fun setupSpinnerFlags(){
        val flags = arrayOf(
            R.drawable.a_ic_russia,
            R.drawable.a_ic_ukraine
        )

        val adapter = CustomAdapter(context,flags)

        countryCodeSpinner.adapter = adapter

       /* if(sharedPreferences!!.contains("noCon") && sharedPreferences!!.getBoolean("noCon",false) || sharedPreferences!!.contains("success") && sharedPreferences!!.getBoolean("success",false)) {
            phoneEdit.setText(sharedPreferences!!.getString("phoneNumber",""))

        }else{ */
            countryCodeSpinner.onItemSelectedListener = object : OnItemSelectedListener {

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                    when (position) {
                        0 -> phoneEdit.mask = "+7(###)###-##-##"
                        1 -> phoneEdit.mask = "+34(###)###-##-##"
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }

        //}

    }

    private fun setupSpinnerPassengers(){
        val passengerTypes = arrayOf("1 пассажир", "2 пассажира", "3 пассажира", "4 пассажира", "5 пассажиров", "Более 5 пассажиров")

        val adapter = ArrayAdapter<String>(context!!,
            R.layout.spinner_item, passengerTypes)
        adapter.setDropDownViewResource(R.layout.spinner_drop_down_item)
        spinner.adapter = adapter

        spinner.setSelection(1)
    }

    private fun setupSpinnerContactMethod(){
        val contactMethods = arrayOf("Выберите удобный тип связи", "WhatsApp", "Telegram", "Viber", "Позвонить на телефон")


        val adapter = SpinnerAdapter(context!!,
            R.layout.spinner_item_contact,contactMethods)
        adapter.isEnabled(0)

        adapter.setDropDownViewResource(R.layout.spinner_drop_down_item)
        spinnerContact.setSelection(1)
        spinnerContact.adapter = adapter

        spinnerContact.onItemSelectedListener = object : OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                try{
                    if (position != 0) {
                     (parent!!.getChildAt(0) as? TextView)!!.setTextColor(ContextCompat.getColor(context!!,
                         R.color.black
                     ))
                    }
                }catch (e:KotlinNullPointerException){
                    e.printStackTrace()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun validateFields():Boolean{

        return (editFrom.text.toString() != "" && editTo.text.toString() != "" && spinnerContact!!.selectedItemPosition != 0 && (phoneEdit!!.text!!.length == phoneEdit!!.mask.length && !phoneEdit!!.text!!.contains("X")))
    }


    private fun switchFragment(fragment:Fragment){
      //  typeWriterView.removeAnimation()

        activity!!.supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment, fragment)
            .addToBackStack(null)
            .commit()
    }


    override fun onResume() {
        super.onResume()

        /*

        чтобы анимация не останавливалась, но бывают какие-то странные баги с подвисанием и вылетом один раз в 20 попыток - походу проблемы самой либы

       в этом случае, на строке 44 нужно убрать initAnim()


        typeWriterView!!.text = differentSentences[3]

        Handler().postDelayed({
            initAnim()
        }, 2000)


         */
    }

/*    override fun onStop() {
        super.onStop()

        typeWriterView!!.removeAnimation()
        typeWriterView!!.text = differentSentences[3]
    }*/

    override fun onPause() {
        super.onPause()

        typeWriterView!!.removeAnimation()
        typeWriterView!!.text = differentSentences[3]
    }


}

