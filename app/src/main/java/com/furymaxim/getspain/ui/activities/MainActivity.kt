package com.furymaxim.getspain.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.furymaxim.getspain.R
import com.furymaxim.getspain.ui.fragments.TransferFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        attachFragment()
    }

    private fun attachFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(
                R.id.fragment,
                TransferFragment()
            )
            .commit()
    }
}



